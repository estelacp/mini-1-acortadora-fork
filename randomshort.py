#!/usr/bin/python3

"""
 randomshort class
 Simple web application to trim URLs
"""

import webapp
from urllib.parse import unquote

form = """
    <br/><br/>
    <form action="" method = "POST"> 
    <label for="url">url :</label>
    <input type="text" name="url" value=""><br>
    <label for="short">short :</label>
    <input type="text" name="short" value="">
    <input type="submit" value="Enviar">
    </form>
"""


class randomshort(webapp.webApp):
    """Simple web application to trim URLs.

    urls is stored in a dictionary, which is empty to introduce trimmed urls."""

    urls = {}

    def parse(self, request):
        """Return the resource name (including /)"""
        method = request.split(' ', 2)[0]
        resource = request.split(' ', 2)[1]
        body = unquote(request.split('\r\n\r\n', 1)[1])

        return method, resource, body

    def process(self, petition):

        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        method, resource, body = petition
        if method == "GET":
            if resource == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body>page to trim urls <br> urls available: " \
                           + str(self.urls)
            elif resource in self.urls:
                httpCode = "308 Permanent Redirect"
                htmlBody = '<meta http-equiv="refresh" content="1;url=' + self.urls[resource] + '">'

            else:
                httpCode = "404 Not Found"
                htmlBody = "<html><body>No redirect available. Use the resource '/'"

        else:
            url = unquote(body.split("&")[0].split("=")[1])
            short = "/" + body.split("&")[1].split("=")[1]
            if url != "" and short != "/":
                if url.startswith('http://') or url.startswith('https://'):
                    self.urls[short] = url
                    httpCode = "200 OK"
                    htmlBody = "<html><body>original url: " \
                               + '<a href="' + self.urls[short] + ' ">"' + self.urls[short] + '"</a><br>' \
                        + "url trimmed: "'<a href="' + self.urls[short] + ' ">"' + short + '"</a>'
                else:
                    url = "https://" + url
                    self.urls[short] = url
                    httpCode = "200 OK"
                    htmlBody = "<html><body>original url: " \
                               + '<a href="' + self.urls[short] + ' ">"' + self.urls[short] + '"</a><br>' \
                        + "url trimmed: " + '<a href="' + self.urls[short] + ' ">"' + short + '"</a>'

            else:
                httpCode = "200 OK"
                htmlBody = "<html><body>404 Not found. Fill in all form fields, please. <br>"

        htmlBody += "<br> Form: <br>" + form + "</body></html>"

        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = randomshort("localhost", 1234)
